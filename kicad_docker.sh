#!/bin/bash
# Script for running KiCad in docker with all necessary parameters
#
# Author:   Martin Stejskal
# Created:  2018.12.07
# Modified: 2018.12.07

# Do you want to remove container when exited, or want to keep it? We recommend
# to keep container, since your settings will be kept. But in some cases you
# might want to start from zero.
remove_container_after_exit=0

# Define directory which will be mounted "from host" to "container".
# If you do not know what this is, just be happy with fact that "your computer"
# will be in "host" in your KiCad's home directory.
mount_host_from="/"
mount_container_where="/home/$(whoami)/host"

##############################################################################
# DO NOT TOUCH UNLESS YOU KNOW WHAT ARE YOU DOING !!!                        #
##############################################################################
if [ "$remove_container_after_exit" -gt 0 ] ; then
  remove="--rm"
else
  remove=""
fi

# Check if image need to be created
$( docker images -a | grep kicad_img > /dev/null )
if [ $? -ne 0 ] ; then
    # Need to create image
    echo "Need to build image. Building..."
    docker build \
      --build-arg UNAME=$(whoami) \
      --build-arg UID=$(id -u) \
      --build-arg GID=$(id -g) \
      -t kicad_img .
    if [ $? -gt 0 ] ; then
        echo "Build failed :("
        exit 1
    fi
else
    echo "The kicad_img already exists. Skipping building"
fi

# `--ti` Run in interactive mode
# `--rm` Delete container once it is closed
# `-e ` Pass environment variable to container
# It is mandatory to mount `/tmp/.X11-unix` as well as `.Xauthority`
# `--net=host` Allow to communicate with host network - essential for X11
#   sockets

# If "Kicad" container already exists, use it. Else create new container
$( docker ps -a | grep KiCad > /dev/null )
if [ $? -eq 0 ] ; then
    # Container exists -> check "remove" option
    if [ "$remove_container_after_exit" -gt 0 ] ; then
        # remove container & recreate
        docker rm KiCad &&
        docker run -ti $remove \
            -e DISPLAY=$DISPLAY \
            -v /tmp/.X11-unix:/tmp/.X11-unix \
            -v $HOME/.Xauthority:/home/$(whoami)/.Xauthority \
            --net=host \
	    --user $(id -u):$(id -g) \
            -v "$mount_host_from":"$mount_container_where" \
            --name KiCad kicad_img
    else
        # Remove option is disabled -> jsut run
        docker start KiCad
    fi
else
    # Container does not exists -> create
    docker run -ti $remove \
        -e DISPLAY=$DISPLAY \
        -v /tmp/.X11-unix:/tmp/.X11-unix \
        -v $HOME/.Xauthority:/home/$(whoami)/.Xauthority \
        --net=host \
	--user $(id -u):$(id -g) \
        -v "$mount_host_from":"$mount_container_where" \
        --name KiCad kicad_img
fi

if [ $? -ne 0 ] ; then
  echo "If you have issue with DISPLAY variable, maybe you just need to run:"\
      " xhost +"
fi
